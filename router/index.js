import express from 'express';
export const router = express.Router();
import conexion from '../models/conexion.js';
import alumnosDb from "../models/alumnos.js";

router.get('/', (req, res) => {
    res.render('index', { titulo: "Mi Primer Pagina ejs", nombre: "Estrella Landeros" });
});

router.get('/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    }
    res.render('tabla', params);
});

router.post('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render('tabla', params);
});

// Ruteo de cotizacion.ejs
router.get('/cotizacion', (req, res) => {
    res.render('cotizacion');
});

router.post('/cotizacion', (req, res) => {
    const { precio, porcentajeInicial, plazo } = req.body;

    // Validar que el porcentaje de pago inicial no sea negativo ni supere el 100%
    if (parseFloat(porcentajeInicial) < 0) {
        const error = 'El porcentaje de pago inicial no puede ser negativo.';
        return res.render('cotizacion', { error });
    }

    if (parseFloat(porcentajeInicial) > 100) {
        const error = 'El porcentaje de pago inicial no puede superar el 100%.';
        return res.render('cotizacion', { error });
    }

    // Validar que el precio y plazo no sean negativos
    if (parseFloat(precio) < 0 || parseInt(plazo) < 0) {
        const error = 'El precio y el plazo no pueden ser negativos.';
        return res.render('cotizacion', { error });
    }

    // Resto del cálculo
    const pagoInicial = (parseFloat(porcentajeInicial) / 100) * parseFloat(precio);
    const totalFinanciar = parseFloat(precio) - pagoInicial;
    const pagoMensual = totalFinanciar / parseInt(plazo);

    res.render('cotizacion', { 
        porcentajeInicial,
        precio,
        plazo,
        pagoInicial,
        totalFinanciar,
        pagoMensual
    });
});

let rows;
router.get('/alumnos', async (req, res) => {
    try {
        rows = await alumnosDb.mostrarTodos();
        res.render('alumnos', { reg: rows });
    } catch (error) {
        console.error(error);
        res.status(500).send("Ocurrió un error: " + error);
    }
});

let params;
router.post('/alumnos', async (req, res) => {
    try {
        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        };
        const registros = await alumnosDb.insertar(params);
        console.log("-------------- registros ", registros);

    } catch (error) {
        console.error(error);
        res.status(400).send("Sucedio un error: " + error);
    }
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', { reg: rows });
});

async function prueba(){
    try {
        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es ", res);
    } catch (error) { 
        console.log("Surgió un error", error);
    } finally {
        conexion.end();
    }
}
    
export default {router};
